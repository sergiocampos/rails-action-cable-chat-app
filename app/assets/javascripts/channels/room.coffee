App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # alert data['message']
    jQuery('#messages').append data['message']

  speak: (msg) ->
    @perform 'speak', message: msg

  jQuery(document).on 'keypress', '[data-behavior~=room_speaker]', (evt) ->
    if evt.keyCode is 13 # return = send
      App.room.speak evt.target.value
      evt.target.value = ''
      evt.preventDefault()